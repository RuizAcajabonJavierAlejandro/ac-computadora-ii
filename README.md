# COMPUTADORAS ELECTRÓNICAS
```plantuml
@startmindmap
* COMPUTADORAS ELECTRONICAS
**_ Son
***[#lightgreen] Máquina capaz de efectuar una secuencia de operaciones mediante \n un programa, de tal manera, que se realice un procesamiento sobre \n un conjunto de datos de entrada, obteniéndose otro conjunto \n de datos de salida.
****[#lightblue] ANTECEDENTES
*****_ Como
****** Computadoras electromecánicas
*******_ Funcionaban con
******** Relés
*********_ ejemplo
********** Harvard Mark I \n 3500 relés
****[#lightblue] SURGIMIENTO
*****_ Debido a 
****** Necesidad de automatización
****** Auge de la industria
****** Ciencia e ingeniería 
****** Crecimiento del manejo de datos
****** Rapida obsolescencia de la generación 0
****[#lightblue] CARACTERISTICAS
***** 1a Generación
****** Tubos al vacio
*******_ como la
******** Colossus Mk I \n ENIAC
***** 2a Generación
****** Transistores
*******_ como la
******** IBM 608
@endmindmap
```mindmap
```
# ARQUITECTURA VON NEUHMAN Y HARVARD
```plantuml
@startmindmap
* VON NEUMANN Y HARVARD
**[#lightgreen] VON NEUMANN
***[#lightblue] CARACTERISTICAS
****_ como
***** Ejecución de instrucciones
***** Dispositivo de control
***** Operar y almacenar resultado
***** Interpretar significado
***** Recolección de instrucciones de memoria
***** Extracción de datos de la memoria
***[#lightblue] MÁQUINA SECUENCIAL
**** Carga de programas
**** Tarea de cómputo
**** Secuenciador de control
***[#lightblue] IMPLEMENTACIÓN 
**** Memoria
**** Unidad de e/s
**** Cálculo(cpu)
***** Control
***** Registros
***[#lightblue] PROGRAMA ALMACENADO
**** Secuencias
**** Iteraciones simples
**** Carga de datos
***[#lightblue] COMUNICACIÓN 
**** Sistema de buses
***** Datos
***** Direcciones
***** Control
**[#lightgreen] HARVARD
*** Arquitectura de computadora con pistas de almacenamiento y de señal \n físicamente separadas para las instrucciones y para los datos
****[#lightblue] Memoria de instrucciones
*****_ Almacena
****** Instrucciones del programa a ejecutar
***** Se implementa con memorias no volátiles
****[#lightblue] Memoria de datos
*****_ Almacena
****** Datos del programa a ejecutar
***** Al variar los datos se usa la RAM
****[#lightblue] CPU
***** Control
****** Lee instrucciones de la memoria 
***** Aritmetica-lógica
****** Ejecuta las instrucciones
***** Registros
****[#lightblue] Sistema de entrada/salida
@endmindmap
```mindmap
```
# BASURA ELECTRÓNICA
```plantuml
@startmindmap
* BASURA ELECTRÓNICA
**_ Es
***[#lightgreen]  Productos eléctricos o electrónicos que  \n han sido desechados o descartados, \n tales como: ordenadores, teléfonos móviles, \n televisores y electrodomésticos. 
****[#lightblue] CAUSAS
*****_ Entre ellas
****** Evolución tecnológica
****** Desfase veloz
****** Alto consumo
****** Obsolescencia programada
****[#lightblue] CONSECUENCIAS
*****_ Como
****** -Contaminantes tóxicos \n -Sustancias cancerígenas
*******_ Que suponen
******** Riesgos y costes
*********_ A nivel
********** Social
********** Ambiental
********** Económico
****[#lightblue] ORIGEN
*****_ Proviene de
****** Paises desarrollados
*******_ y termina en
******** Paises pobres
****[#lightblue] SOLUCIONES
*****_ Posiblemente 
****** Reciclado de equipos
****** Donación de equipos
****** Reducción de sustancias primas
****** Legislación en materia de tratamiento de residuos
****** Responsabilidad de uso del usuario
@endmindmap
```mindmap
